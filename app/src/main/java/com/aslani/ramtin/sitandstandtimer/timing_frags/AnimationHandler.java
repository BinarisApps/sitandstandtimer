package com.aslani.ramtin.sitandstandtimer.timing_frags;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.graphics.Interpolator;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.OvershootInterpolator;

import cimi.com.easeinterpolator.EaseCubicInOutInterpolator;
import cimi.com.easeinterpolator.EaseCubicOutInterpolator;


public class AnimationHandler {

    public static ObjectAnimator FadeInAnim(View viewToFadeIn, int duration) {
        // A simple method for fading in a view, returns an ObjectAnimator to be used in another class
        ObjectAnimator fadeInAnim = ObjectAnimator.ofFloat(viewToFadeIn, View.ALPHA, 0, 1);
        fadeInAnim.setDuration(duration);
        fadeInAnim.setInterpolator(new EaseCubicInOutInterpolator());
        return fadeInAnim;
    }

    public static ObjectAnimator FadeOutAnim(View viewToFadeIn, int duration) {
        // A simple method for fading in a view, returns an ObjectAnimator to be used in another class
        ObjectAnimator fadeOutAnim = ObjectAnimator.ofFloat(viewToFadeIn, View.ALPHA, 0);
        fadeOutAnim.setDuration(duration);
        fadeOutAnim.setInterpolator(new EaseCubicInOutInterpolator());
        return fadeOutAnim;
    }


    public static ObjectAnimator[] ScaleAnim(View viewToScale, TimeInterpolator interpolator, int duration, float startXValue, float endXValue, float startYValue, float endYValue) {
        // A simple method for scaling in a view by changing scale properties, returns an array of x and y ObjectAnimator to be used in another class
        ObjectAnimator bounceInXAnim = ObjectAnimator.ofFloat(viewToScale, View.SCALE_X, startXValue, endXValue);
        bounceInXAnim.setDuration(duration);
        bounceInXAnim.setInterpolator(interpolator);

        ObjectAnimator bounceInYAnim = ObjectAnimator.ofFloat(viewToScale, View.SCALE_Y, startYValue, endYValue);
        bounceInYAnim.setDuration(duration);
        bounceInYAnim.setInterpolator(interpolator);

        ObjectAnimator[] animArray = new ObjectAnimator[]{bounceInXAnim, bounceInYAnim};

        return animArray;
    }

    public static ObjectAnimator[] ScaleAnimReturn(View viewToScale, TimeInterpolator interpolator, int duration, float startXValue, float endXValue, float startYValue, float endYValue) {
        // A simple method for scaling in a view by changing scale properties and then returning to previous value, returns an array of x and y ObjectAnimator to be used in another class
        ObjectAnimator bounceInXAnim = ObjectAnimator.ofFloat(viewToScale, View.SCALE_X, startXValue, endXValue);
        bounceInXAnim.setDuration(duration);
        bounceInXAnim.setInterpolator(interpolator);
        bounceInXAnim.setRepeatCount(1);
        bounceInXAnim.setRepeatMode(ValueAnimator.REVERSE);

        ObjectAnimator bounceInYAnim = ObjectAnimator.ofFloat(viewToScale, View.SCALE_Y, startYValue, endYValue);
        bounceInYAnim.setDuration(duration);
        bounceInYAnim.setInterpolator(interpolator);
        bounceInYAnim.setRepeatCount(1);
        bounceInYAnim.setRepeatMode(ValueAnimator.REVERSE);

        ObjectAnimator[] animArray = new ObjectAnimator[]{bounceInXAnim, bounceInYAnim};

        return animArray;
    }

    public static ObjectAnimator RotateAnim(View viewToBounceIn, TimeInterpolator interpolator, int duration, float startValue, float endValue) {
        // A simple method for rotating a view on its x axis by changing rotation properties, returns ObjectAnimator to be used in another class
        ObjectAnimator rotateInXAnim = ObjectAnimator.ofFloat(viewToBounceIn, View.ROTATION, startValue, endValue);
        rotateInXAnim.setDuration(duration);
        rotateInXAnim.setInterpolator(interpolator);
        return rotateInXAnim;
    }

    public static ObjectAnimator SlideY(View viewToSlideIn, TimeInterpolator interpolator, int duration, float yStartValue, float yEndValue) {
        // A method for sliding a view on the y axis, returns an ObjectAnimator to be used in another class
        ObjectAnimator slideAnim = ObjectAnimator.ofFloat(viewToSlideIn, View.TRANSLATION_Y, yStartValue, yEndValue);
        slideAnim.setDuration(duration);
        slideAnim.setInterpolator(interpolator);
        return slideAnim;
    }

    public static ObjectAnimator SlideX(View viewToSlideIn, TimeInterpolator interpolator, int duration, float xStartValue, float xEndValue) {
        // A method for sliding a view on the y axis, returns an ObjectAnimator to be used in another class
        ObjectAnimator slideAnim = ObjectAnimator.ofFloat(viewToSlideIn, View.TRANSLATION_X, xStartValue, xEndValue);
        slideAnim.setDuration(duration);
        slideAnim.setInterpolator(interpolator);
        return slideAnim;
    }
}
