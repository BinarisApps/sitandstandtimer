package com.aslani.ramtin.sitandstandtimer.timing_frags;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentStanding();
            case 1:
                return new FragmentSitting();
            default:
                return new FragmentStanding();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
