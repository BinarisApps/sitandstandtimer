package com.aslani.ramtin.sitandstandtimer.timing_frags;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aslani.ramtin.sitandstandtimer.R;

import cimi.com.easeinterpolator.EaseBackInInterpolator;
import cimi.com.easeinterpolator.EaseBackOutInterpolator;
import cimi.com.easeinterpolator.EaseCubicOutInterpolator;


public class FragmentStanding extends Fragment {
    ViewPager mPager;
    AnimatorSet masterOutSet;
    AnimatorSet masterInSet;
    AnimatorSet pulseSet;
    Activity MainTimingActivity;
    private ImageView ring1;
    private ImageView ring2;
    private ImageView ring3;
    private ImageView leftArrow;
    private ImageView avatarimage;
    private TextView bottomSwipeText;
    private TextView standingText;
    private float bottomTextInitYvalue;
    private float ring1InitXValue;
    private float ring2InitXValue;
    private float ring3InitXValue;
    private float leftArrowInitXValue;
    private int lastPage;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("Message");

            // Cases for different messages to call the different methods of this class
            switch (message) {
                case "StartCountdown":
                    AnimateOutView();
                    break;
                case "StopCountdown":
                    AnimateInView();
                    break;
                case "UnregisterListener":
                    UnregisterBroadcastListener();
                    break;
                case "Female":
                    avatarimage.setImageResource(R.drawable.ic_female_avatar_standing);
                    break;
                case "Male":
                    avatarimage.setImageResource(R.drawable.ic_male_avatar_standing);
                    break;
            }
        }
    };

    public FragmentStanding() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_standing, container, false);
        MainTimingActivity = getActivity();

        InitializeAnimationViews(rootview);

        AnimateInView();

        lastPage = PrefUtils.getInt(MainTimingActivity, -1, "com.icantstandsitting.PAGER_PREF");
        // Register mMessageReceiver to receive messages.

        if (lastPage == 1) {
            AnimateOutView();
        }

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(MainTimingActivity).registerReceiver(mMessageReceiver,
                new IntentFilter("CountdownEvent"));

        return rootview;
    }

    private void InitializeAnimationViews(View v) {
        avatarimage = (ImageView) v.findViewById(R.id.stand_avatar);
        ring1 = (ImageView) v.findViewById(R.id.ring1);
        ring2 = (ImageView) v.findViewById(R.id.ring2);
        ring3 = (ImageView) v.findViewById(R.id.ring3);
        leftArrow = (ImageView) v.findViewById(R.id.arrow_left);
        bottomSwipeText = (TextView) v.findViewById(R.id.swipe_text);
        standingText = (TextView) v.findViewById(R.id.standing_text);

        bottomTextInitYvalue = bottomSwipeText.getTranslationY();
        ring1InitXValue = ring1.getTranslationY();
        ring2InitXValue = ring2.getTranslationY();
        ring3InitXValue = ring3.getTranslationY();
        leftArrowInitXValue = leftArrow.getTranslationY();
    }

    private void KillAnimatorsets() {
        if (masterInSet != null) {
            if (masterInSet.isRunning()) {
                masterInSet.removeAllListeners();
                masterInSet.cancel();
            }
        }
        if (masterOutSet != null) {
            if (masterOutSet.isRunning()) {
                masterOutSet.removeAllListeners();
                masterOutSet.cancel();
            }
        }
        if (pulseSet != null) {
            if (pulseSet.isRunning()) {
                pulseSet.removeAllListeners();
                pulseSet.cancel();
            }
        }
    }

    public void UnregisterBroadcastListener() {
        // Unregister since the activity is being finished by user
        KillAnimatorsets();
        LocalBroadcastManager.getInstance(MainTimingActivity).unregisterReceiver(mMessageReceiver);
    }

    private void AnimateInView() {
        // Animates in all the views in the layout and starts the looping animations when done.
        KillAnimatorsets();

        // Check avatar selected in settings and set accordingly
        String avatarSelected = PrefUtils.getString(MainTimingActivity, "Male", "com.icantstandsitting.AvatarSelected");
        if (avatarSelected.equals("Female")) {
            avatarimage.setImageResource(R.drawable.ic_female_avatar_standing);
        } else {
            avatarimage.setImageResource(R.drawable.ic_male_avatar_standing);
        }
        masterInSet = new AnimatorSet();

        // --------------------------
        // Bottom Elements
        // --------------------------

        bottomSwipeText.setTranslationY(bottomTextInitYvalue + 400);

        // Bottom Text animation
        ObjectAnimator bottomTextTranslateAnim = AnimationHandler.SlideY(bottomSwipeText, new EaseCubicOutInterpolator(), 750, bottomSwipeText.getTranslationY(), bottomTextInitYvalue);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeInAnim(bottomSwipeText, 750);

        masterInSet.playTogether(bottomTextTranslateAnim, bottomTextFadeAnim);

        // ----------------------
        // Bottom Rings & Arrow
        // ----------------------


        ring1.setTranslationX(ring1InitXValue + 1500);
        ring2.setTranslationX(ring2InitXValue + 1500);
        ring3.setTranslationX(ring3InitXValue + 1500);
        leftArrow.setTranslationX(leftArrowInitXValue + 2000);

        // Ring 1 animation
        ObjectAnimator ring1Anim = AnimationHandler.SlideX(ring1, new EaseBackOutInterpolator(), 550, ring1.getTranslationX(), ring1InitXValue);
        // Ring 2 animation
        ObjectAnimator ring2Anim = AnimationHandler.SlideX(ring2, new EaseBackOutInterpolator(), 550, ring2.getTranslationX(), ring2InitXValue);
        // Ring 3 animation
        ObjectAnimator ring3Anim = AnimationHandler.SlideX(ring3, new EaseBackOutInterpolator(), 550, ring3.getTranslationX(), ring3InitXValue);
        // left arrow animation
        ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(leftArrow, new EaseBackOutInterpolator(), 550, leftArrow.getTranslationX(), leftArrowInitXValue);


        AnimatorSet RingAnimSet = new AnimatorSet();
        RingAnimSet.play(ring1Anim);
        RingAnimSet.play(ring2Anim).after(200);
        RingAnimSet.play(ring3Anim).after(400);
        RingAnimSet.play(leftArrowAnim).after(600);

        masterInSet.play(RingAnimSet).after(200);

        // -----------------
        //  Middle Elements
        // -----------------

        avatarimage.setScaleX(0);
        avatarimage.setScaleY(0);

        standingText.setScaleX(0);
        standingText.setScaleY(0);

        // SitAvatar animation
        ObjectAnimator[] sitAvatarAnim = AnimationHandler.ScaleAnim(avatarimage, new EaseBackOutInterpolator(), 650, 0, 1, 0, 1);

        // SittingText animation
        ObjectAnimator[] sittingTextAnim = AnimationHandler.ScaleAnim(standingText, new EaseBackOutInterpolator(), 650, 0, 1, 0, 1);


        AnimatorSet middleRingSet = new AnimatorSet();
        middleRingSet.play(sitAvatarAnim[0]).after(600).with(sitAvatarAnim[1]).after(600);
        middleRingSet.play(sittingTextAnim[0]).after(800).with(sittingTextAnim[1]).after(800);

        // -------------------
        // Master AnimatorSet
        // -------------------

        masterInSet.play(middleRingSet);

        masterInSet.start();

        masterInSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                PulseCircles();
            }
        });
    }

    private void AnimateOutView() {
        // Animates out all the views in the layout and stops the looping animations.
        // Used when pages are switched to "unload" previous page

        KillAnimatorsets();

        masterOutSet = new AnimatorSet();

        // ------------------
        //  Bottom Elements
        // ------------------


        // Bottom Text animation
        ObjectAnimator bottomTextTranslateAnim = AnimationHandler.SlideY(bottomSwipeText, new EaseCubicOutInterpolator(), 750, bottomSwipeText.getTranslationY(), bottomTextInitYvalue + 400);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeOutAnim(bottomSwipeText, 750);

        // AnimatorSet
        AnimatorSet BottomAnimSet = new AnimatorSet();
        BottomAnimSet.playTogether(bottomTextTranslateAnim, bottomTextFadeAnim);

        // ----------------------
        // Bottom Rings & Arrow
        // ----------------------

        // Ring 1 animation
        ObjectAnimator ring1Anim = AnimationHandler.SlideX(ring1, new EaseBackOutInterpolator(), 550, ring1.getTranslationX(), ring1InitXValue - 800);
        // Ring 2 animation
        ObjectAnimator ring2Anim = AnimationHandler.SlideX(ring2, new EaseBackOutInterpolator(), 550, ring2.getTranslationX(), ring2InitXValue - 800);
        // Ring 3 animation
        ObjectAnimator ring3Anim = AnimationHandler.SlideX(ring3, new EaseBackOutInterpolator(), 550, ring3.getTranslationX(), ring3InitXValue - 800);
        // left arrow animation
        ObjectAnimator rightArrowAnim = AnimationHandler.SlideX(leftArrow, new EaseBackOutInterpolator(), 550, leftArrow.getTranslationX(), leftArrowInitXValue - 800);

        AnimatorSet RingAnimSet = new AnimatorSet();
        RingAnimSet.play(ring1Anim);
        RingAnimSet.play(ring2Anim).after(200);
        RingAnimSet.play(ring3Anim).after(400);
        RingAnimSet.play(rightArrowAnim).after(600);

        BottomAnimSet.play(RingAnimSet).after(400);


        // -------------------------
        // Circle & Middle Elements
        // -------------------------


        avatarimage.setScaleX(0);
        avatarimage.setScaleY(0);
        standingText.setScaleX(0);
        standingText.setScaleY(0);


        AnimatorSet topTextAnimSet = new AnimatorSet();

        // -------------------
        // Master AnimatorSet
        // -------------------

        masterOutSet.play(BottomAnimSet);
        masterOutSet.play(topTextAnimSet).after(500);
        masterOutSet.start();
    }


    private void PulseCircles() {

        if (pulseSet != null) {
            if (pulseSet.isRunning()) {
                pulseSet.removeAllListeners();
                pulseSet.cancel();
            }
        }

        pulseSet = new AnimatorSet();

        // Rings and arrow pulse
        ObjectAnimator[] ring1Anim = AnimationHandler.ScaleAnimReturn(ring1, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

        ObjectAnimator[] ring2Anim = AnimationHandler.ScaleAnimReturn(ring2, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

        ObjectAnimator[] ring3Anim = AnimationHandler.ScaleAnimReturn(ring3, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

        ObjectAnimator rightArrowAnim = AnimationHandler.SlideX(leftArrow, new EaseCubicOutInterpolator(), 550, leftArrow.getTranslationX(), leftArrowInitXValue - 10);
        rightArrowAnim.setRepeatMode(ValueAnimator.REVERSE);
        rightArrowAnim.setRepeatCount(1);

        pulseSet.playTogether(ring1Anim[0], ring1Anim[1]);
        pulseSet.play(ring2Anim[0]).after(200).with(ring1Anim[1]).after(200);
        pulseSet.play(ring3Anim[0]).after(400).with(ring3Anim[1]).after(400);
        pulseSet.play(rightArrowAnim).after(900);

        pulseSet.start();

        pulseSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                pulseSet.start();
            }
        });
    }

}
