package com.aslani.ramtin.sitandstandtimer.timing_frags;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aslani.ramtin.sitandstandtimer.R;


public class MainTimingActivity extends AppCompatActivity {

    public static int lastPage;
    ViewPager TimingPager;
    ViewPagerAdapter TimingAdapter;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("Message");

            // Cases for different messages to call the different methods of this class
            switch (message) {
                case "SetPage0":
                    SetViewpagerPage(0);
                    break;
                case "SetPage1":
                    SetViewpagerPage(1);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_timing);

        TimingPager = (ViewPager) findViewById(R.id.pager);
        TimingAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TimingPager.setAdapter(TimingAdapter);

        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("FragmentEvent"));

        lastPage = PrefUtils.getInt(this, -1, "com.icantstandsitting.PAGER_PREF");
        if (lastPage != -1) {
            SetViewpagerPage(lastPage);
        }
        TimingPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0) {
                    // page selected = standing
                    lastPage = 0;
                    // stop countdown.
                    sendMessage("StopCountdown");
                    PrefUtils.setInt(getApplicationContext(), lastPage, "com.icantstandsitting.PAGER_PREF");
                } else if (i == 1) {
                    // page selected = sitting
                    lastPage = 1;
                    // Start Countdown
                    sendMessage("StartCountdown");
                    PrefUtils.setInt(getApplicationContext(), lastPage, "com.icantstandsitting.PAGER_PREF");
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    // Send an Intent with an action.
    private void sendMessage(String Message) {
        Intent intent = new Intent("CountdownEvent");
        // add data
        intent.putExtra("Message", Message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        lastPage = PrefUtils.getInt(this, -1, "com.icantstandsitting.PAGER_PREF");
        if (lastPage != -1) {
            SetViewpagerPage(lastPage);
        }
    }

    public void SetViewpagerPage(int page) {
        TimingPager.setCurrentItem(page, true);
    }
}

class PrefUtils {

    public static int getInt(Context context, int defValue, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref.contains(Key)) {
            return pref.getInt(Key, defValue);
        } else return defValue;
    }

    public static boolean setInt(Context context, int value, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(Key, value);
        return editor.commit();
    }

    public static boolean getBoolean(Context context, boolean defValue, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref.contains(Key)) {
            return pref.getBoolean(Key, defValue);
        } else return defValue;
    }

    public static boolean setBoolean(Context context, boolean value, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Key, value);
        return editor.commit();
    }

    public static String getString(Context context, String defValue, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref.contains(Key)) {
            return pref.getString(Key, defValue);
        } else return defValue;
    }
}
