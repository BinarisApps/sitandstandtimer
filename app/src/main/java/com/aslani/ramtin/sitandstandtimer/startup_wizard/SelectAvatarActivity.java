package com.aslani.ramtin.sitandstandtimer.startup_wizard;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aslani.ramtin.sitandstandtimer.R;
import com.aslani.ramtin.sitandstandtimer.timing_frags.AnimationHandler;
import com.aslani.ramtin.sitandstandtimer.timing_frags.MainTimingActivity;
import com.wangjie.wavecompat.WaveCompat;
import com.wangjie.wavecompat.WaveDrawable;

import cimi.com.easeinterpolator.EaseBackOutInterpolator;
import cimi.com.easeinterpolator.EaseCircularOutInterpolator;
import cimi.com.easeinterpolator.EaseCubicOutInterpolator;
import cimi.com.easeinterpolator.EaseElasticOutInterpolator;
import io.codetail.animation.arcanimator.ArcAnimator;
import io.codetail.animation.arcanimator.Side;

public class SelectAvatarActivity extends AppCompatActivity {

    FloatingActionButton floatButton;

    ImageView maleAvatar;
    ImageView femaleAvatar;

    TextView bottomText;

    Activity app;

    TextView maleText;
    TextView femaleText;
    boolean buttonClicked = false;
    private String avatarSelected;
    private AnimatorSet buttonSet;

    // variable to track event time
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_avatar);

        initializeViews();

        final int CurrentApiLevel = Build.VERSION.SDK_INT;

        app = this;

        floatButton.setScaleX(0);
        floatButton.setScaleY(0);

        AnimateIn();

        femaleAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Preventing multiple clicks, using threshold of 300 milliseconds
                if (Math.abs(SystemClock.elapsedRealtime() - mLastClickTime) < 300) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                avatarSelected = "Female";

                // Animate elevation
                AnimateElevation((int) ViewCompat.getElevation(femaleAvatar), 100, 400, femaleAvatar, new EaseCircularOutInterpolator());
                AnimateElevation((int) ViewCompat.getElevation(maleAvatar), 0, 200, maleAvatar, new EaseCircularOutInterpolator());

                if (CurrentApiLevel < 21) {
                    setAvatarBackground(femaleAvatar, maleAvatar);
                }


                if (!buttonClicked) {
                    buttonClicked = true;
                    //Animate in FAB
                    AnimateInFAB();
                }
            }
        });

        maleAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Preventing multiple clicks, using threshold of 300 milliseconds
                if (Math.abs(SystemClock.elapsedRealtime() - mLastClickTime) < 300) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                avatarSelected = "Male";
                // Animate bounce
                AnimateElevation((int) ViewCompat.getElevation(femaleAvatar), 0, 200, femaleAvatar, new EaseCircularOutInterpolator());
                AnimateElevation((int) ViewCompat.getElevation(maleAvatar), 100, 400, maleAvatar, new EaseCircularOutInterpolator());

                if (CurrentApiLevel < 21) {
                    setAvatarBackground(maleAvatar, femaleAvatar);
                }

                if (!buttonClicked) {
                    buttonClicked = true;
                    //Animate in FAB
                    AnimateInFAB();
                }
            }
        });

        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Save avatarSelected and IntroDone to preferences and start a new activity (MainTimingActivity) with circular reveal
                setBoolean(app, true, "com.icantstandsitting.IntroDone");
                setString(app, avatarSelected, "com.icantstandsitting.AvatarSelected");
                setString(app, "60 Min", "com.icantstandsitting.CountdownLength");
                AnimateToNextActivity();
            }
        });
    }

    void initializeViews() {
        floatButton = (FloatingActionButton) findViewById(R.id.FAB);
        maleAvatar = (ImageView) findViewById(R.id.male_avatar);
        femaleAvatar = (ImageView) findViewById(R.id.female_avatar);
        bottomText = (TextView) findViewById(R.id.select_text);
        femaleText = (TextView) findViewById(R.id.female_text);
        maleText = (TextView) findViewById(R.id.male_text);

        floatButton.setEnabled(false);
    }


    void AnimateIn() {
        // Animate in all the views
        AnimatorSet masterSet = new AnimatorSet();

        maleAvatar.setScaleX(0);
        maleAvatar.setScaleY(0);

        femaleAvatar.setScaleX(0);
        femaleAvatar.setScaleY(0);

        float bottomTextInit = bottomText.getTranslationY();

        bottomText.setTranslationY(bottomTextInit + 500);

        float maleTextInit = maleText.getTranslationY();
        float femaleTextInit = femaleText.getTranslationY();

        maleText.setTranslationY(maleTextInit + 900);
        femaleText.setTranslationY(femaleTextInit + 900);

        ObjectAnimator bottomTextSlideAnim = AnimationHandler.SlideY(bottomText, new EaseCubicOutInterpolator(), 750, bottomText.getTranslationY(), bottomTextInit);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeInAnim(bottomText, 750);

        ObjectAnimator maleTextSlideAnim = AnimationHandler.SlideY(maleText, new EaseCubicOutInterpolator(), 750, maleText.getTranslationY(), maleTextInit);
        ObjectAnimator maleTextFadeAnim = AnimationHandler.FadeInAnim(maleText, 750);
        ObjectAnimator femaleTextSlideAnim = AnimationHandler.SlideY(femaleText, new EaseCubicOutInterpolator(), 750, femaleText.getTranslationY(), femaleTextInit);
        ObjectAnimator femaleTextFadeAnim = AnimationHandler.FadeInAnim(femaleText, 750);

        ObjectAnimator[] maleScaleAnim = AnimationHandler.ScaleAnim(maleAvatar, new EaseBackOutInterpolator(), 500, 0, 1, 0, 1);
        ObjectAnimator[] femaleScaleAnim = AnimationHandler.ScaleAnim(femaleAvatar, new EaseBackOutInterpolator(), 500, 0, 1, 0, 1);

        masterSet.playTogether(bottomTextSlideAnim, bottomTextFadeAnim);
        masterSet.play(maleScaleAnim[0]).after(200).with(maleScaleAnim[1]).after(200);
        masterSet.play(maleTextSlideAnim).after(150).with(maleTextFadeAnim).after(150);
        masterSet.play(femaleScaleAnim[0]).after(750).with(femaleScaleAnim[1]).after(750);
        masterSet.play(femaleTextSlideAnim).after(650).with(femaleTextFadeAnim).after(650);

        masterSet.start();
    }

    private void AnimateElevation(float startvalue, float endvalue, int duration, final ImageView image, TimeInterpolator interpolator) {
        ValueAnimator animator = new ValueAnimator().ofFloat(startvalue, endvalue);
        animator.setInterpolator(interpolator);
        animator.setDuration(duration);
        animator.start();
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewCompat.setElevation(image, (Float) animation.getAnimatedValue());
            }
        });
    }

    void AnimateInFAB() {
        floatButton.setEnabled(true);
        ObjectAnimator[] fabScaleAnim = AnimationHandler.ScaleAnim(floatButton, new EaseElasticOutInterpolator(800), 800, 0, 1, 0, 1);
        fabScaleAnim[0].start();
        fabScaleAnim[1].start();
    }

    void AnimateToNextActivity() {

        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        final int width = metrics.widthPixels;
        final int height = metrics.heightPixels;

        // Fade out all the other views
        ObjectAnimator fadeBottom = AnimationHandler.FadeOutAnim(bottomText, 500);
        ObjectAnimator fadeMaleAvatar = AnimationHandler.FadeOutAnim(maleAvatar, 500);
        ObjectAnimator fadeMaleText = AnimationHandler.FadeOutAnim(maleText, 500);
        ObjectAnimator fadeFemaleAvatar = AnimationHandler.FadeOutAnim(femaleAvatar, 500);
        ObjectAnimator fadeFemaleText = AnimationHandler.FadeOutAnim(femaleText, 500);
        AnimatorSet animSet = new AnimatorSet();
        animSet.playTogether(fadeBottom, fadeMaleAvatar, fadeMaleText, fadeFemaleText, fadeFemaleAvatar);
        animSet.start();

        // Arc the FAB into middle of screen
        ArcAnimator arc = ArcAnimator.createArcAnimator(floatButton, width / 2, height / 2, 120, Side.RIGHT);
        arc.setDuration(600);
        arc.setInterpolator(new EaseCubicOutInterpolator());
        arc.start();
        arc.addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                super.onAnimationEnd(animation);
                WaveCompat.startWaveFilterForResult(app, new WaveDrawable().setColor(0xfff44336).setTouchPoint(width / 2, height / 2), generateIntent(), 1245);
            }
        });
    }

    private Intent generateIntent() {
        Intent intent = new Intent(this, MainTimingActivity.class);
        return intent;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1245) {
            finish();
        }
    }

    private boolean setBoolean(Context context, boolean value, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Key, value);
        return editor.commit();
    }

    private boolean setString(Context context, String value, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Key, value);
        return editor.commit();
    }

    private void setAvatarBackground(ImageView imageToSet, ImageView imageToRemove) {
        imageToSet.setBackgroundColor(ContextCompat.getColor(app, R.color.accent));
        imageToRemove.setBackgroundColor(ContextCompat.getColor(app, R.color.primary));

    }
}
