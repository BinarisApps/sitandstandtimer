package com.aslani.ramtin.sitandstandtimer.startup_wizard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import com.aslani.ramtin.sitandstandtimer.R;
import com.aslani.ramtin.sitandstandtimer.timing_frags.AnimationHandler;
import com.eftimoff.androipathview.PathView;
import com.wangjie.wavecompat.WaveCompat;
import com.wangjie.wavecompat.WaveDrawable;

import cimi.com.easeinterpolator.EaseCubicOutInterpolator;
import cimi.com.easeinterpolator.EaseElasticOutInterpolator;
import cimi.com.easeinterpolator.EaseSineOutInterpolator;
import io.codetail.animation.arcanimator.ArcAnimator;
import io.codetail.animation.arcanimator.Side;

public class WelcomeActivity extends AppCompatActivity {

    Activity app;
    private PathView pathView;
    private FloatingActionButton btnFloat;
    private TextView topText;
    private TextView middleText;
    private float initTopY;
    private float initMiddleY;
    private boolean animDone = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        initializeViews();

        AnimateIn();

        app = this;

        btnFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (animDone) {
                    AnimateToNextActivity();
                }
            }
        });
    }

    void initializeViews() {
        pathView = (PathView) findViewById(R.id.logo_path);
        btnFloat = (FloatingActionButton) findViewById(R.id.FAB_bottom);
        topText = (TextView) findViewById(R.id.top_textView);
        middleText = (TextView) findViewById(R.id.middle_text);

        initTopY = topText.getTranslationY();
        initMiddleY = middleText.getTranslationY();

        topText.setTranslationY(initTopY - 550);
        middleText.setTranslationY(initMiddleY + 1900);

        btnFloat.setScaleX(0);
        btnFloat.setScaleY(0);

        btnFloat.setEnabled(false);
    }


    void AnimateIn() {
        // Animate in path view
        pathView.getPathAnimator()
                .delay(500)
                .duration(3000)
                .interpolator(new EaseSineOutInterpolator())
                .listenerEnd(new PathView.AnimatorBuilder.ListenerEnd() {
                    @Override
                    public void onAnimationEnd() {
                        AnimateInRest();
                    }
                })
                .start();

    }

    void AnimateInRest() {
        // Animate in the rest
        AnimatorSet masterSet = new AnimatorSet();

        ObjectAnimator topTextSlideAnim = AnimationHandler.SlideY(topText, new EaseCubicOutInterpolator(), 750, topText.getTranslationY(), initTopY);
        ObjectAnimator topTextFadeAnim = AnimationHandler.FadeInAnim(topText, 750);

        final ObjectAnimator middlepTextSlideAnim = AnimationHandler.SlideY(middleText, new EaseCubicOutInterpolator(), 750, middleText.getTranslationY(), initMiddleY);
        final ObjectAnimator middleTextFadeAnim = AnimationHandler.FadeInAnim(middleText, 750);

        ObjectAnimator[] floatBtnScaleAnim = AnimationHandler.ScaleAnim(btnFloat, new EaseElasticOutInterpolator(800), 800, 0, 1, 0, 1);

        final ObjectAnimator pathViewFadeAnim = AnimationHandler.FadeOutAnim(pathView, 500);

        masterSet.playTogether(topTextFadeAnim, topTextSlideAnim, floatBtnScaleAnim[0], floatBtnScaleAnim[1]);
        masterSet.start();

        topTextSlideAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                AnimatorSet wrapUpSet = new AnimatorSet();
                wrapUpSet.playTogether(middlepTextSlideAnim, middleTextFadeAnim, pathViewFadeAnim);
                wrapUpSet.start();
                wrapUpSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        btnFloat.setEnabled(true);
                        animDone = true;
                    }
                });
            }
        });
    }

    void AnimateToNextActivity() {

        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        final int width = metrics.widthPixels;
        final int height = metrics.heightPixels;

        // Fade out all the other views
        ObjectAnimator fadeTop = AnimationHandler.FadeOutAnim(topText, 500);
        ObjectAnimator fadeMiddle = AnimationHandler.FadeOutAnim(middleText, 500);
        AnimatorSet animSet = new AnimatorSet();
        animSet.playTogether(fadeTop, fadeMiddle);
        animSet.start();

        // Arc the FAB into middle of screen
        ArcAnimator arc = ArcAnimator.createArcAnimator(btnFloat, width / 2, height / 2, 120, Side.RIGHT);
        arc.setDuration(600);
        arc.setInterpolator(new EaseCubicOutInterpolator());
        arc.start();
        arc.addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                super.onAnimationEnd(animation);
                WaveCompat.startWaveFilterForResult(app, new WaveDrawable().setColor(0xff448AFF).setTouchPoint(width / 2, height / 2), generateIntent(), 249);

            }
        });
    }

    private Intent generateIntent() {
        Intent intent = new Intent(this, SelectAvatarActivity.class);
        return intent;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 249) {
            finish();
        }
    }
}
