package com.aslani.ramtin.sitandstandtimer.timing_frags;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aslani.ramtin.sitandstandtimer.R;
import com.aslani.ramtin.sitandstandtimer.SettingsActivity;

import java.util.concurrent.TimeUnit;

import at.grabner.circleprogress.CircleProgressView;
import cimi.com.easeinterpolator.EaseBackInInterpolator;
import cimi.com.easeinterpolator.EaseBackOutInterpolator;
import cimi.com.easeinterpolator.EaseCubicOutInterpolator;
import cimi.com.easeinterpolator.EaseElasticOutInterpolator;


public class FragmentSitting extends Fragment {
    public Intent serviceIntent;
    AnimatorSet masterOutSet;
    AnimatorSet masterInSet;
    AnimatorSet pulseSet;
    ViewPager mPager;
    private CircleProgressView mCircleView;
    // ----------------------
    // Views For Animations
    // ----------------------
    private Button quitButton;
    private Button settingsButton;
    private ImageView bottomBG;
    private TextView MinNrText;
    private TextView MinuteText;
    private TextView bottomSwipeText;
    private ImageView ring1;
    private ImageView ring2;
    private ImageView ring3;

    // ------------------------------
    // Stored Values For Animations
    // ------------------------------
    private ImageView rightArrow;
    private ImageView bgCircle;
    private ImageView bgRedCircle;
    private ImageView sitAvatar;
    private TextView sittingText;
    private float btnInitYValue;
    private float bgInitYvalue;
    private float bottomTextInitYvalue;
    private float ring1InitXValue;
    private float ring2InitXValue;
    private float ring3InitXValue;
    private float rightArrowInitXValue;
    private float MinNrTextInitYValue;
    private float MinuteTextInitYValue;
    private float sitAvatarInitYValue;
    private float sitAvatarInitRotationValue;
    private float MinNrTextInitSize;
    private float MinuteTextInitSize;
    private Activity MainTimingActivity;
    private boolean isServiceRunning;
    private boolean isAlarmRinging;
    private boolean AnimDone = false;
    private int lastPage;
    private int pulseTick;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("Message");
            Long Extra = intent.getLongExtra("Extra", 0);

            // Cases for different messages to call the different methods of this class
            switch (message) {
                // case "Finished": timer is finished. update gui
                case "Finished":
                    isAlarmRinging = true;
                    AnimateIntoAlarmFromTimerView();
                    break;
                case "Update Circle":
                    if (AnimDone) // if fade in animation is done
                    {
                        mCircleView.setValueAnimated(3600 - TimeUnit.MILLISECONDS.toSeconds(Extra));
                    }
                    break;
                case "Update Text":
                    if (AnimDone) {
                        MinNrText.setText("" + TimeUnit.MILLISECONDS.toMinutes(Extra));
                        if (TimeUnit.MILLISECONDS.toMinutes(Extra) == 0) {
                            MinNrText.setText("1");
                        }
                        PulseCircles();
                    }
                    break;
                case "Standing":
                    sendMessage("SetPage0");
                    lastPage = PrefUtils.getInt(MainTimingActivity, -1, "com.icantstandsitting.PAGER_PREF");
                    if (lastPage != 0 || lastPage != -1) {
                        AnimateOutView();
                    }
                    break;
                case "Sitting":
                    sendMessage("SetPage1");
                    lastPage = PrefUtils.getInt(MainTimingActivity, -1, "com.icantstandsitting.PAGER_PREF");
                    if (lastPage != 1) {
                        AnimateInView();
                    }
                    break;
                case "StartCountdown":
                    StartCountdownHandler();
                    AnimateInView();
                    break;
                case "StopCountdown":
                    AnimateOutView();
                    StopCountdownHandler();
                    break;
                case "UnregisterListener":
                    UnregisterBroadcastListener();
                    break;
                case "KillService":
                    KillService();
                    break;
                case "Female":
                    sitAvatar.setImageResource(R.drawable.ic_avatar_female_sitting);
                    break;
                case "Male":
                    sitAvatar.setImageResource(R.drawable.ic_avatar_male_sitting);
                    break;
                case "Countdown Pref":
                    StopCountdownHandler();
                    StartCountdownHandler();
                    break;
            }
        }
    };


    public FragmentSitting() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_sitting, container, false);
        MainTimingActivity = getActivity();

        mCircleView = (CircleProgressView) v.findViewById(R.id.circleprogress);

        mCircleView.setMaxValue(3600);
        pulseTick = 0;

        InitializeAnimationViews(v);

        bgRedCircle.setScaleX(10);
        bgRedCircle.setScaleX(10);

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go to settings activity
                Intent intent = new Intent(MainTimingActivity, SettingsActivity.class);
                startActivity(intent);
            }
        });

        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StopCountdownHandler();
                UnregisterBroadcastListener();
                lastPage = 0;
                PrefUtils.setInt(MainTimingActivity, lastPage, "com.icantstandsitting.PAGER_PREF");
                KillService();
                KillAnimatorsets();
                MainTimingActivity.finish();
            }
        });

        if (PrefUtils.getBoolean(MainTimingActivity, false, "com.icantstandsitting.isServiceRunning")) // If value exists in sharedprefs and is not false
        {
            isServiceRunning = true;
        }

        if (PrefUtils.getBoolean(MainTimingActivity, false, "com.icantstandsitting.FINISHED_PREF")) // If value exists in sharedprefs and is not false
        {
            isAlarmRinging = true;
            AnimateInAlarmView();
        }

        if (!isAlarmRinging) {
            AnimateInView();
        }

        lastPage = PrefUtils.getInt(MainTimingActivity, -1, "com.icantstandsitting.PAGER_PREF");
        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(MainTimingActivity).registerReceiver(mMessageReceiver,
                new IntentFilter("CountdownEvent"));

        if (lastPage == -1 || lastPage == 0) {
            AnimateOutView();

        }

        return v;
    }

    private void KillAnimatorsets() {
        if (masterInSet != null) {
            if (masterInSet.isRunning()) {
                masterInSet.removeAllListeners();
                masterInSet.cancel();
            }
        }
        if (masterOutSet != null) {
            if (masterOutSet.isRunning()) {
                masterOutSet.removeAllListeners();
                masterOutSet.cancel();
            }
        }
        if (pulseSet != null) {
            if (pulseSet.isRunning()) {
                pulseSet.removeAllListeners();
                pulseSet.cancel();
            }
        }
    }

    private void InitializeAnimationViews(View view) {
        // Initializes all the different views that are used for animations
        MinNrText = (TextView) view.findViewById(R.id.MinuteNRText);
        MinuteText = (TextView) view.findViewById(R.id.minutetxt);
        bottomSwipeText = (TextView) view.findViewById(R.id.swipetochange);
        sittingText = (TextView) view.findViewById(R.id.sitting_text);

        quitButton = (Button) view.findViewById(R.id.quit_btn);
        settingsButton = (Button) view.findViewById(R.id.settings_btn);

        bottomBG = (ImageView) view.findViewById(R.id.bottom_text_bg);

        ring1 = (ImageView) view.findViewById(R.id.ring1);
        ring2 = (ImageView) view.findViewById(R.id.ring2);
        ring3 = (ImageView) view.findViewById(R.id.ring3);
        rightArrow = (ImageView) view.findViewById(R.id.arrow_right);

        bgCircle = (ImageView) view.findViewById(R.id.bgcircle);
        bgRedCircle = (ImageView) view.findViewById(R.id.bgredcircle);
        sitAvatar = (ImageView) view.findViewById(R.id.sit_avatar);

        btnInitYValue = quitButton.getTranslationY();
        bgInitYvalue = bottomBG.getTranslationY();
        bottomTextInitYvalue = bottomSwipeText.getTranslationY();

        ring1InitXValue = ring1.getTranslationX();
        ring2InitXValue = ring2.getTranslationX();
        ring3InitXValue = ring3.getTranslationX();
        rightArrowInitXValue = rightArrow.getTranslationX();

        MinuteTextInitYValue = MinuteText.getTranslationY();
        MinuteTextInitSize = MinuteText.getTextSize();
        MinNrTextInitYValue = MinNrText.getTranslationY();
        MinNrTextInitSize = MinNrText.getTextSize();

        sitAvatarInitYValue = sitAvatar.getTranslationY();
        sitAvatarInitRotationValue = sitAvatar.getRotation();
    }

    private void AnimateIntoAlarmFromTimerView() {
        KillAnimatorsets();

        masterInSet = new AnimatorSet();

        // MinNrText animation
        ObjectAnimator MinNrTextSlideAnim = AnimationHandler.SlideY(MinNrText, new EaseCubicOutInterpolator(), 750, MinNrText.getTranslationY(), MinNrTextInitYValue - 300);
        ObjectAnimator MinNrTextFadeAnim = AnimationHandler.FadeOutAnim(MinNrText, 750);

        // MinuteText animation
        ObjectAnimator MinuteTextSlideAnim = AnimationHandler.SlideY(MinuteText, new EaseCubicOutInterpolator(), 750, MinuteText.getTranslationY(), MinuteTextInitYValue - 600);
        ObjectAnimator MinuteTextFadeAnim = AnimationHandler.FadeInAnim(MinuteText, 750);

        AnimatorSet topTextAnimSet = new AnimatorSet();
        topTextAnimSet.playTogether(MinNrTextSlideAnim, MinNrTextFadeAnim);
        topTextAnimSet.play(MinuteTextSlideAnim).after(200).with(MinuteTextFadeAnim).after(200);

        // Bottom BG animation
        ObjectAnimator bottomBGAnim = AnimationHandler.SlideY(bottomBG, new EaseBackOutInterpolator(), 750, settingsButton.getTranslationY(), btnInitYValue + 700);
        ObjectAnimator bottomBGFadeAnim = AnimationHandler.FadeInAnim(bottomBG, 350);

        // Bottom Text animation
        ObjectAnimator bottomTextTranslateAnim = AnimationHandler.SlideY(bottomSwipeText, new EaseCubicOutInterpolator(), 750, bottomSwipeText.getTranslationY(), bottomTextInitYvalue + 400);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeOutAnim(bottomSwipeText, 750);

        // AnimatorSet
        AnimatorSet bottomTextAnimSet = new AnimatorSet();
        bottomTextAnimSet.playTogether(bottomTextTranslateAnim, bottomTextFadeAnim);

        // ----------------------
        // Bottom Rings & Arrow
        // ----------------------

        // Ring 1 animation
        ObjectAnimator ring1Anim = AnimationHandler.SlideX(ring1, new EaseBackOutInterpolator(), 550, ring1.getTranslationX(), ring1InitXValue + 800);
        // Ring 2 animation
        ObjectAnimator ring2Anim = AnimationHandler.SlideX(ring2, new EaseBackOutInterpolator(), 550, ring2.getTranslationX(), ring2InitXValue + 800);
        // Ring 3 animation
        ObjectAnimator ring3Anim = AnimationHandler.SlideX(ring3, new EaseBackOutInterpolator(), 550, ring3.getTranslationX(), ring3InitXValue + 800);
        // left arrow animation
        ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(rightArrow, new EaseBackOutInterpolator(), 550, rightArrow.getTranslationX(), rightArrowInitXValue + 800);

        AnimatorSet RingAnimSet = new AnimatorSet();
        RingAnimSet.play(ring1Anim);
        RingAnimSet.play(ring2Anim).after(200);
        RingAnimSet.play(ring3Anim).after(400);
        RingAnimSet.play(leftArrowAnim).after(600);

        bottomTextAnimSet.play(RingAnimSet);

        // Quit button animation
        ObjectAnimator quitBtnAnim = AnimationHandler.SlideY(quitButton, new EaseBackOutInterpolator(), 600, quitButton.getTranslationY(), btnInitYValue + 200);

        // Settings button animation
        ObjectAnimator settingsBtnAnim = AnimationHandler.SlideY(settingsButton, new EaseBackOutInterpolator(), 600, settingsButton.getTranslationY(), btnInitYValue + 200);

        masterInSet.play(topTextAnimSet).with(bottomBGAnim).with(bottomBGFadeAnim);

        ObjectAnimator[] bgRedCircleAnim = AnimationHandler.ScaleAnim(bgRedCircle, new EaseBackOutInterpolator(), 950, 1, 10, 1, 10);
        AnimatorSet bgRedCircleSet = new AnimatorSet();
        bgRedCircleSet.playTogether(bgRedCircleAnim[0], bgRedCircleAnim[1]);

        // SitAvatar animation
        ObjectAnimator[] sitAvatarAnim = AnimationHandler.ScaleAnim(sitAvatar, new EaseBackOutInterpolator(), 650, sitAvatar.getScaleX(), 0, sitAvatar.getScaleY(), 0);

        // SittingText animation
        ObjectAnimator[] sittingTextAnim = AnimationHandler.ScaleAnim(sittingText, new EaseBackOutInterpolator(), 650, sittingText.getScaleX(), 0, sittingText.getScaleY(), 0);

        masterInSet.play(bgRedCircleAnim[0]).after(400).with(bgRedCircleAnim[1]).after(400);
        masterInSet.playTogether(sitAvatarAnim[0], sitAvatarAnim[1]);
        masterInSet.playTogether(sittingTextAnim[0], sittingTextAnim[1]);
        masterInSet.playTogether(quitBtnAnim, settingsBtnAnim);
        masterInSet.play(bottomTextAnimSet);
        masterInSet.start();

        masterInSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                AnimateInAlarmView();
            }
        });
    }

    private void AnimateInAlarmView() {
        KillAnimatorsets();

        sitAvatar.setImageResource(R.drawable.ic_icon_alarm);

        MinNrText.setTextColor(ContextCompat.getColor(MainTimingActivity, R.color.icons));
        MinNrText.setTextSize(38);
        MinNrText.setText("Enough Sitting");
        MinuteText.setTextColor(ContextCompat.getColor(MainTimingActivity, R.color.primary_light));
        MinuteText.setTextSize(24);
        MinuteText.setText("Time to get up!");
        bottomSwipeText.setTextColor(ContextCompat.getColor(MainTimingActivity, R.color.icons));

        bgRedCircle.setScaleX(10);
        bgRedCircle.setScaleY(10);

        mCircleView.setScaleX(0);
        mCircleView.setScaleY(0);
        bgCircle.setScaleX(0);
        bgCircle.setScaleY(0);
        sittingText.setScaleX(0);
        sittingText.setScaleY(0);

        quitButton.setTranslationY(btnInitYValue + 200);
        settingsButton.setTranslationY(btnInitYValue + 200);

        bottomBG.setY(bgInitYvalue + 700);

        masterInSet = new AnimatorSet();

        bottomSwipeText.setTranslationY(bottomTextInitYvalue + 400);

        // Bottom Text animation
        ObjectAnimator bottomTextTranslateAnim = AnimationHandler.SlideY(bottomSwipeText, new EaseCubicOutInterpolator(), 750, bottomSwipeText.getTranslationY(), bottomTextInitYvalue);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeInAnim(bottomSwipeText, 750);

        masterInSet.playTogether(bottomTextTranslateAnim, bottomTextFadeAnim);

        ring1.setTranslationX(ring1InitXValue - 1500);
        ring2.setTranslationX(ring2InitXValue - 1500);
        ring3.setTranslationX(ring3InitXValue - 1500);
        rightArrow.setTranslationX(rightArrowInitXValue - 2000);

        // Ring 1 animation
        ObjectAnimator ring1Anim = AnimationHandler.SlideX(ring1, new EaseBackOutInterpolator(), 550, ring1.getTranslationX(), ring1InitXValue);
        // Ring 2 animation
        ObjectAnimator ring2Anim = AnimationHandler.SlideX(ring2, new EaseBackOutInterpolator(), 550, ring2.getTranslationX(), ring2InitXValue);
        // Ring 3 animation
        ObjectAnimator ring3Anim = AnimationHandler.SlideX(ring3, new EaseBackOutInterpolator(), 550, ring3.getTranslationX(), ring3InitXValue);
        // left arrow animation
        ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(rightArrow, new EaseBackOutInterpolator(), 550, rightArrow.getTranslationX(), rightArrowInitXValue);

        AnimatorSet RingAnimSet = new AnimatorSet();
        RingAnimSet.play(ring1Anim);
        RingAnimSet.play(ring2Anim).after(200);
        RingAnimSet.play(ring3Anim).after(400);
        RingAnimSet.play(leftArrowAnim).after(600);

        masterInSet.play(RingAnimSet);

        MinNrText.setTranslationY(MinNrTextInitYValue - 300);
        MinuteText.setTranslationY(MinuteTextInitYValue - 600);

        // MinNrText animation
        ObjectAnimator MinNrTextSlideAnim = AnimationHandler.SlideY(MinNrText, new EaseCubicOutInterpolator(), 750, MinNrText.getTranslationY(), MinNrTextInitYValue + 45);
        ObjectAnimator MinNrTextFadeAnim = AnimationHandler.FadeInAnim(MinNrText, 750);

        // MinuteText animation
        ObjectAnimator MinuteTextSlideAnim = AnimationHandler.SlideY(MinuteText, new EaseCubicOutInterpolator(), 750, MinuteText.getTranslationY(), MinuteTextInitYValue - 75);
        ObjectAnimator MinuteTextFadeAnim = AnimationHandler.FadeInAnim(MinuteText, 750);

        AnimatorSet topTextAnimSet = new AnimatorSet();
        topTextAnimSet.playTogether(MinNrTextSlideAnim, MinNrTextFadeAnim);
        topTextAnimSet.play(MinuteTextSlideAnim).after(200).with(MinuteTextFadeAnim).after(200);

        masterInSet.play(topTextAnimSet);

        sitAvatar.setScaleX(0);
        sitAvatar.setScaleY(0);
        sitAvatar.setTranslationY(sitAvatarInitYValue + 90);

        // SitAvatar (Alarm Icon) animation
        ObjectAnimator[] sitAvatarAnim = AnimationHandler.ScaleAnim(sitAvatar, new EaseBackOutInterpolator(), 550, 0, 1.55f, 0, 1.55f);

        masterInSet.play(sitAvatarAnim[0]).after(200).with(sitAvatarAnim[1]).after(200);

        masterInSet.start();

        masterInSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                PulseAlarmElements();
            }
        });

    }

    private void AnimateInView() {
        // Animates in all the views in the layout and starts the looping animations when done.
        KillAnimatorsets();

        // Check avatar selected in settings and set accordingly
        String avatarSelected = PrefUtils.getString(MainTimingActivity, "Male", "com.icantstandsitting.AvatarSelected");
        if (avatarSelected.equals("Female")) {
            sitAvatar.setImageResource(R.drawable.ic_avatar_female_sitting);
        } else {
            sitAvatar.setImageResource(R.drawable.ic_avatar_male_sitting);
        }

        masterInSet = new AnimatorSet();
        AnimDone = false;
        mCircleView.setValue(3600);
        MinNrText.setText("0");
        mCircleView.setSpinBarColor(ContextCompat.getColor(MainTimingActivity, R.color.primary));

        MinNrText.setTextColor(ContextCompat.getColor(MainTimingActivity, R.color.divider));
        MinNrText.setTextSize(TypedValue.COMPLEX_UNIT_PX, MinNrTextInitSize);

        MinuteText.setTextColor(ContextCompat.getColor(MainTimingActivity, R.color.divider));
        MinuteText.setTextSize(TypedValue.COMPLEX_UNIT_PX, MinuteTextInitSize);
        MinuteText.setText("Minutes");

        bottomSwipeText.setTextColor(ContextCompat.getColor(MainTimingActivity, R.color.divider));
        sitAvatar.setRotation(sitAvatarInitRotationValue);

        // --------------------------
        // Buttons & Bottom Elements
        // --------------------------


        quitButton.setTranslationY(btnInitYValue + 200);
        settingsButton.setTranslationY(btnInitYValue + 200);

        bottomBG.setTranslationY(bgInitYvalue + 700);

        bottomSwipeText.setTranslationY(bottomTextInitYvalue + 400);


        // Quit button animation
        ObjectAnimator quitBtnAnim = AnimationHandler.SlideY(quitButton, new EaseBackOutInterpolator(), 600, quitButton.getTranslationY(), btnInitYValue);

        // Settings button animation
        ObjectAnimator settingsBtnAnim = AnimationHandler.SlideY(settingsButton, new EaseBackOutInterpolator(), 600, settingsButton.getTranslationY(), btnInitYValue);

        // Bottom BG animation
        ObjectAnimator bottomBGAnim = AnimationHandler.SlideY(bottomBG, new EaseBackOutInterpolator(), 750, settingsButton.getTranslationY(), btnInitYValue);
        ObjectAnimator bottomBGFadeAnim = AnimationHandler.FadeInAnim(bottomBG, 350);

        // Bottom Text animation
        ObjectAnimator bottomTextTranslateAnim = AnimationHandler.SlideY(bottomSwipeText, new EaseCubicOutInterpolator(), 750, bottomSwipeText.getTranslationY(), bottomTextInitYvalue);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeInAnim(bottomSwipeText, 750);

        // AnimatorSet
        AnimatorSet BottomAnimSet = new AnimatorSet();
        BottomAnimSet.play(settingsBtnAnim);
        BottomAnimSet.play(quitBtnAnim).after(200);
        BottomAnimSet.play(bottomBGAnim).after(380).with(bottomBGFadeAnim).after(380);

        AnimatorSet bottomTextAnimSet = new AnimatorSet();
        bottomTextAnimSet.playTogether(bottomTextTranslateAnim, bottomTextFadeAnim);

        BottomAnimSet.play(bottomTextAnimSet).after(450);

        // ----------------------
        // Bottom Rings & Arrow
        // ----------------------

        ring1.setTranslationX(ring1InitXValue - 1500);
        ring2.setTranslationX(ring2InitXValue - 1500);
        ring3.setTranslationX(ring3InitXValue - 1500);
        rightArrow.setTranslationX(rightArrowInitXValue - 2000);

        // Ring 1 animation
        ObjectAnimator ring1Anim = AnimationHandler.SlideX(ring1, new EaseBackOutInterpolator(), 550, ring1.getTranslationX(), ring1InitXValue);
        // Ring 2 animation
        ObjectAnimator ring2Anim = AnimationHandler.SlideX(ring2, new EaseBackOutInterpolator(), 550, ring2.getTranslationX(), ring2InitXValue);
        // Ring 3 animation
        ObjectAnimator ring3Anim = AnimationHandler.SlideX(ring3, new EaseBackOutInterpolator(), 550, ring3.getTranslationX(), ring3InitXValue);
        // left arrow animation
        ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(rightArrow, new EaseBackOutInterpolator(), 550, rightArrow.getTranslationX(), rightArrowInitXValue);

        AnimatorSet RingAnimSet = new AnimatorSet();
        RingAnimSet.play(ring1Anim);
        RingAnimSet.play(ring2Anim).after(200);
        RingAnimSet.play(ring3Anim).after(400);
        RingAnimSet.play(leftArrowAnim).after(600);

        BottomAnimSet.play(RingAnimSet).after(800);


        // -------------------------
        // Circle & Middle Elements
        // -------------------------

        mCircleView.setScaleX(0);
        mCircleView.setScaleY(0);
        bgCircle.setScaleX(0);
        bgCircle.setScaleY(0);
        sitAvatar.setScaleX(0);
        sitAvatar.setScaleY(0);
        sittingText.setScaleX(0);
        sittingText.setScaleY(0);

        bgRedCircle.setScaleX(10);
        bgRedCircle.setScaleY(10);


        // CircleProgress animation
        ObjectAnimator[] mCircleProgressAnim = AnimationHandler.ScaleAnim(mCircleView, new EaseBackOutInterpolator(), 750, 0, 1, 0, 1);

        // BGCircle animation
        ObjectAnimator[] bgCircleAnim = AnimationHandler.ScaleAnim(bgCircle, new EaseBackOutInterpolator(), 750, 0, 1, 0, 1);

        // BGRedCircle animation
        ObjectAnimator[] bgRedCircleAnim = AnimationHandler.ScaleAnim(bgRedCircle, new EaseBackOutInterpolator(), 650, 10, 1, 10, 1);

        // SitAvatar animation
        ObjectAnimator[] sitAvatarAnim = AnimationHandler.ScaleAnim(sitAvatar, new EaseBackOutInterpolator(), 650, 0, 1, 0, 1);

        // SittingText animation
        ObjectAnimator[] sittingTextAnim = AnimationHandler.ScaleAnim(sittingText, new EaseBackOutInterpolator(), 650, 0, 1, 0, 1);


        AnimatorSet middleRingSet = new AnimatorSet();
        middleRingSet.playTogether(mCircleProgressAnim[0], mCircleProgressAnim[1]);
        middleRingSet.play(bgCircleAnim[0]).after(130).with(bgCircleAnim[1]).after(130);
        middleRingSet.play(sitAvatarAnim[0]).after(1000).with(sitAvatarAnim[1]).after(1000);
        middleRingSet.play(sittingTextAnim[0]).after(800).with(sittingTextAnim[1]).after(800);

        AnimatorSet bgRedCircleSet = new AnimatorSet();
        bgRedCircleSet.playTogether(bgRedCircleAnim[0], bgRedCircleAnim[1]);

        mCircleProgressAnim[0].addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mCircleView.spin();
                super.onAnimationEnd(animation);
            }
        });
        // ------------------
        // Top Text Elements
        // ------------------

        MinNrText.setTranslationY(MinNrTextInitYValue - 600);
        MinuteText.setTranslationY(MinuteTextInitYValue - 650);

        // MinNrText animation
        ObjectAnimator MinNrTextSlideAnim = AnimationHandler.SlideY(MinNrText, new EaseCubicOutInterpolator(), 750, MinNrText.getTranslationY(), MinNrTextInitYValue);
        ObjectAnimator MinNrTextFadeAnim = AnimationHandler.FadeInAnim(MinNrText, 750);

        // MinuteText animation
        ObjectAnimator MinuteTextSlideAnim = AnimationHandler.SlideY(MinuteText, new EaseCubicOutInterpolator(), 750, MinuteText.getTranslationY(), MinuteTextInitYValue);
        ObjectAnimator MinuteTextFadeAnim = AnimationHandler.FadeInAnim(MinuteText, 750);

        AnimatorSet topTextAnimSet = new AnimatorSet();
        topTextAnimSet.playTogether(MinNrTextSlideAnim, MinNrTextFadeAnim);
        topTextAnimSet.play(MinuteTextSlideAnim).after(200).with(MinuteTextFadeAnim).after(200);

        // -------------------
        // Master AnimatorSet
        // -------------------

        masterInSet.play(BottomAnimSet);
        masterInSet.play(bgRedCircleSet);
        masterInSet.play(middleRingSet).after(1000);
        masterInSet.play(topTextAnimSet).after(1200);
        masterInSet.start();

        leftArrowAnim.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isServiceRunning) {
                    int i = PrefUtils.getInt(MainTimingActivity, -1, "com.icantstandsitting.TIME_PREFS");
                    if (i != -1) // If value exists in sharedprefs
                    {
                        AnimateTextNR((int) TimeUnit.MILLISECONDS.toMinutes(i), (int) TimeUnit.SECONDS.toMillis(2), MinNrText); // assign it to minute text view with animation
                        mCircleView.setValueAnimated(3600 - TimeUnit.MILLISECONDS.toSeconds(i), TimeUnit.SECONDS.toMillis(2));
                    }
                } else {
                    mCircleView.setValueAnimated(0, TimeUnit.SECONDS.toMillis(2));
                    AnimateTextNR(60, (int) TimeUnit.SECONDS.toMillis(2), MinNrText);
                }
                super.onAnimationEnd(animation);
            }
        });

        sittingTextAnim[0].addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                AnimDone = true;
                mCircleView.stopSpinning();
                super.onAnimationEnd(animation);
            }
        });
    }

    private void AnimateOutView() {
        // Animates out all the views in the layout and stops the looping animations.
        // Used when pages are switched to "unload" previous page

        ObjectAnimator AlarmAvatarRotateRightAnim = AnimationHandler.RotateAnim(sitAvatar, new EaseElasticOutInterpolator(300), 700, sitAvatar.getRotation(), sitAvatarInitRotationValue);

        AlarmAvatarRotateRightAnim.start();

        KillAnimatorsets();

        masterOutSet = new AnimatorSet();

        // --------------------------
        // Buttons & Bottom Elements
        // --------------------------

        bottomSwipeText.setTranslationY(bottomTextInitYvalue + 400);


        quitButton.setTranslationY(btnInitYValue + 200);
        settingsButton.setTranslationY(btnInitYValue + 200);

        // Bottom BG
        bottomBG.setTranslationY(bgInitYvalue + 700);

        // Bottom Text animation
        ObjectAnimator bottomTextTranslateAnim = AnimationHandler.SlideY(bottomSwipeText, new EaseCubicOutInterpolator(), 750, bottomSwipeText.getTranslationY(), bottomTextInitYvalue + 400);
        ObjectAnimator bottomTextFadeAnim = AnimationHandler.FadeOutAnim(bottomSwipeText, 750);

        // AnimatorSet
        AnimatorSet BottomAnimSet = new AnimatorSet();

        AnimatorSet bottomTextAnimSet = new AnimatorSet();
        bottomTextAnimSet.playTogether(bottomTextTranslateAnim, bottomTextFadeAnim);

        BottomAnimSet.play(bottomTextAnimSet).after(450);

        // ----------------------
        // Bottom Rings & Arrow
        // ----------------------

        // Ring 1 animation
        ObjectAnimator ring1Anim = AnimationHandler.SlideX(ring1, new EaseBackOutInterpolator(), 550, ring1.getTranslationX(), ring1InitXValue + 800);
        // Ring 2 animation
        ObjectAnimator ring2Anim = AnimationHandler.SlideX(ring2, new EaseBackOutInterpolator(), 550, ring2.getTranslationX(), ring2InitXValue + 800);
        // Ring 3 animation
        ObjectAnimator ring3Anim = AnimationHandler.SlideX(ring3, new EaseBackOutInterpolator(), 550, ring3.getTranslationX(), ring3InitXValue + 800);
        // left arrow animation
        ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(rightArrow, new EaseBackOutInterpolator(), 550, rightArrow.getTranslationX(), rightArrowInitXValue + 800);

        AnimatorSet RingAnimSet = new AnimatorSet();
        RingAnimSet.play(ring1Anim);
        RingAnimSet.play(ring2Anim).after(200);
        RingAnimSet.play(ring3Anim).after(400);
        RingAnimSet.play(leftArrowAnim).after(600);

        BottomAnimSet.play(RingAnimSet).after(400);


        // -------------------------
        // Circle & Middle Elements
        // -------------------------

        mCircleView.setScaleX(0);
        mCircleView.setScaleY(0);
        bgCircle.setScaleX(0);
        bgCircle.setScaleY(0);

        sitAvatar.setScaleX(0);
        sitAvatar.setScaleY(0);
        sittingText.setScaleX(0);
        sittingText.setScaleY(0);

        if (!isAlarmRinging) {
            ObjectAnimator[] bgRedCircleAnim = AnimationHandler.ScaleAnim(bgRedCircle, new EaseBackOutInterpolator(), 950, 1, 10, 1, 10);
            AnimatorSet bgRedCircleSet = new AnimatorSet();
            bgRedCircleSet.playTogether(bgRedCircleAnim[0], bgRedCircleAnim[1]);
            masterOutSet.playTogether(bgRedCircleAnim[0], bgRedCircleAnim[1]);
        } else if (isAlarmRinging) {
            sitAvatar.setTranslationY(sitAvatarInitYValue);
            sitAvatar.setRotation(sitAvatarInitRotationValue);
        }


        // ------------------
        // Top Text Elements
        // ------------------

        // MinNrText animation
        ObjectAnimator MinNrTextSlideAnim = AnimationHandler.SlideY(MinNrText, new EaseCubicOutInterpolator(), 750, MinNrText.getTranslationY(), MinNrTextInitYValue - 300);
        ObjectAnimator MinNrTextFadeAnim = AnimationHandler.FadeOutAnim(MinNrText, 750);

        // MinuteText animation
        ObjectAnimator MinuteTextSlideAnim = AnimationHandler.SlideY(MinuteText, new EaseCubicOutInterpolator(), 750, MinuteText.getTranslationY(), MinuteTextInitYValue - 600);
        ObjectAnimator MinuteTextFadeAnim = AnimationHandler.FadeInAnim(MinuteText, 750);

        AnimatorSet topTextAnimSet = new AnimatorSet();
        topTextAnimSet.playTogether(MinNrTextSlideAnim, MinNrTextFadeAnim);
        topTextAnimSet.play(MinuteTextSlideAnim).after(200).with(MinuteTextFadeAnim).after(200);

        // -------------------
        // Master AnimatorSet
        // -------------------

        masterOutSet.play(BottomAnimSet);
        masterOutSet.play(topTextAnimSet).after(500);
        masterOutSet.start();

        masterOutSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                bgRedCircle.setScaleX(10);
                bgRedCircle.setScaleY(10);
            }
        });


    }

    private void PulseCircles() {

        if (pulseTick >= 2) {
            if (pulseSet != null) {
                if (pulseSet.isRunning()) {
                    pulseSet.removeAllListeners();
                    pulseSet.cancel();
                }
            }

            pulseSet = new AnimatorSet();

            // BGRedCircle animation
            ObjectAnimator[] bgRedCircleAnim = AnimationHandler.ScaleAnimReturn(bgRedCircle, new EaseBackInInterpolator(), 650, 1, 1.15f, 1, 1.15f);

            // BGCircle animation
            ObjectAnimator[] bgCircleAnim = AnimationHandler.ScaleAnimReturn(bgCircle, new EaseBackInInterpolator(), 650, 1, 1.1f, 1, 1.1f);

            // Rings and arrow pulse
            ObjectAnimator[] ring1Anim = AnimationHandler.ScaleAnimReturn(ring1, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

            ObjectAnimator[] ring2Anim = AnimationHandler.ScaleAnimReturn(ring2, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

            ObjectAnimator[] ring3Anim = AnimationHandler.ScaleAnimReturn(ring3, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

            ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(rightArrow, new EaseCubicOutInterpolator(), 550, rightArrow.getTranslationY(), rightArrowInitXValue + 10);
            leftArrowAnim.setRepeatMode(ValueAnimator.REVERSE);
            leftArrowAnim.setRepeatCount(1);


            pulseSet.playTogether(bgRedCircleAnim[0], bgRedCircleAnim[1]);
            pulseSet.play(bgCircleAnim[0]).after(300).with(bgCircleAnim[1]).after(300);

            pulseSet.playTogether(ring1Anim[0], ring1Anim[1]);
            pulseSet.play(ring2Anim[0]).after(200).with(ring1Anim[1]).after(200);
            pulseSet.play(ring3Anim[0]).after(400).with(ring3Anim[1]).after(400);
            pulseSet.play(leftArrowAnim).after(900);

            pulseSet.start();

            pulseTick = 0;

        } else {
            pulseTick += 1;
        }
    }

    private void PulseAlarmElements() {
        if (pulseSet != null) {
            if (pulseSet.isRunning()) {
                pulseSet.removeAllListeners();
                pulseSet.cancel();
            }
        }
        pulseSet = new AnimatorSet();

        // AlertIcon animation
        ObjectAnimator AlarmAvatarRotateRightAnim = AnimationHandler.RotateAnim(sitAvatar, new EaseElasticOutInterpolator(300), 700, 0, 20);
        ObjectAnimator AlarmAvatarRotateLeftAnim = AnimationHandler.RotateAnim(sitAvatar, new EaseElasticOutInterpolator(300), 700, 0, -20);


        // Rings and arrow pulse
        ObjectAnimator[] ring1Anim = AnimationHandler.ScaleAnimReturn(ring1, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

        ObjectAnimator[] ring2Anim = AnimationHandler.ScaleAnimReturn(ring2, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

        ObjectAnimator[] ring3Anim = AnimationHandler.ScaleAnimReturn(ring3, new EaseBackInInterpolator(), 550, 1, 1.14f, 1, 1.14f);

        ObjectAnimator leftArrowAnim = AnimationHandler.SlideX(rightArrow, new EaseCubicOutInterpolator(), 550, rightArrow.getTranslationY(), rightArrowInitXValue + 10);
        leftArrowAnim.setRepeatMode(ValueAnimator.REVERSE);
        leftArrowAnim.setRepeatCount(1);

        pulseSet.play(AlarmAvatarRotateRightAnim);
        pulseSet.play(AlarmAvatarRotateLeftAnim).after(1000);

        pulseSet.playTogether(ring1Anim[0], ring1Anim[1]);
        pulseSet.play(ring2Anim[0]).after(200).with(ring1Anim[1]).after(200);
        pulseSet.play(ring3Anim[0]).after(400).with(ring3Anim[1]).after(400);
        pulseSet.play(leftArrowAnim).after(900);

        pulseSet.start();

        pulseSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                pulseSet.start();
            }
        });
    }

    // Send an Intent with an action.
    private void sendMessage(String Message) {
        Intent intent = new Intent("FragmentEvent");
        // add data
        intent.putExtra("Message", Message);
        LocalBroadcastManager.getInstance(MainTimingActivity).sendBroadcast(intent);
    }


    public void StartCountdownHandler() {
        serviceIntent = new Intent(MainTimingActivity, CountdownHandler.class);
        serviceIntent.putExtra("StopCountdown", false);
        isServiceRunning = true;
        PrefUtils.setBoolean(MainTimingActivity, isServiceRunning, "com.icantstandsitting.isServiceRunning");

        MainTimingActivity.startService(serviceIntent);
    }

    public void UnregisterBroadcastListener() {
        // Unregister since the activity is being finished by user
        KillAnimatorsets();
        LocalBroadcastManager.getInstance(MainTimingActivity).unregisterReceiver(mMessageReceiver);
    }

    public void StopCountdownHandler() {
        //Cancel countdown and stops service
        serviceIntent = new Intent(MainTimingActivity, CountdownHandler.class);
        serviceIntent.putExtra("StopCountdown", true);
        MainTimingActivity.startService(serviceIntent);
        isServiceRunning = false;
        PrefUtils.setBoolean(MainTimingActivity, isServiceRunning, "com.icantstandsitting.isServiceRunning");
        isAlarmRinging = false;
        PrefUtils.setBoolean(MainTimingActivity, isAlarmRinging, "com.icantstandsitting.FINISHED_PREF");
        mCircleView.setValue(3600);
    }

    public void KillService() {
        MainTimingActivity.stopService(serviceIntent);
        isServiceRunning = false;
    }

    private void AnimateTextNR(int count, int duration, final TextView MinNrText) {
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(0, count);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                MinNrText.setText(String.valueOf(animation.getAnimatedValue()));
            }
        });
        animator.setEvaluator(new TypeEvaluator<Integer>() {
            public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
                return Math.round(startValue + (endValue - startValue) * fraction);
            }
        });
        animator.setDuration(duration);
        animator.start();
    }

    @Override
    public void onDestroy() {
        PrefUtils.setBoolean(MainTimingActivity, isServiceRunning, "com.icantstandsitting.isServiceRunning");
        UnregisterBroadcastListener();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
