package com.aslani.ramtin.sitandstandtimer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aslani.ramtin.sitandstandtimer.startup_wizard.SelectAvatarActivity;
import com.aslani.ramtin.sitandstandtimer.startup_wizard.WelcomeActivity;
import com.aslani.ramtin.sitandstandtimer.timing_frags.MainTimingActivity;


public class MainActivity extends AppCompatActivity {

    public static boolean getBoolean(Context context, boolean defValue, String Key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        if (pref.contains(Key)) {
            return pref.getBoolean(Key, defValue);
        } else return defValue;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Check if setup wizard should run and do so. Otherwise, display timing frags with corresponding activity
        if (getBoolean(this, false, "com.icantstandsitting.IntroDone")) {
            Intent intent = new Intent(getApplicationContext(), MainTimingActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
