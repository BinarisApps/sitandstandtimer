package com.aslani.ramtin.sitandstandtimer;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

public class SettingsActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content,
                        new MainSettingsFragment()).commit();


    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        // Send broadcast to classes to update
        if (key.equals("com.icantstandsitting.CountdownLength")) {
            sendMessage("Countdown Pref", false);
        }
        if (key.equals("com.icantstandsitting.AvatarSelected")) {
            String value = prefs.getString(key, "Male");
            if (value.equals("Male")) {
                sendMessage("Male", true);
            } else {
                sendMessage("Female", true);
            }
        }
    }

    // Send an Intent with an action.
    private void sendMessage(String Message, boolean CountdownEvent) {

        Intent intent = new Intent("CountdownEvent");

        // add data
        intent.putExtra("Message", Message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    public static class MainSettingsFragment extends PreferenceFragment {

        private Activity app;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            app = getActivity();

            Preference feedbackButton = findPreference("feedback_btn");
            feedbackButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    // --SUMMARY--
                    // Send an email intent to binarisapps@gmail.com


                    //Create the Intent
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", "binarisapps@gmail.com", null));

                    /* Fill it with Data */
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback on I Can't Stand Sitting");
                    intent.putExtra(Intent.EXTRA_TEXT, "");

                    /* Send it off to the Activity-Chooser */
                    app.startActivity(Intent.createChooser(intent, "Choose an Email client:"));
                    return true;
                }
            });

            Preference ratingButton = findPreference("rating_btn");
            ratingButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    GoToPlayStoreAndRate();

                    return true;
                }
            });
        }

        private void GoToPlayStoreAndRate() {
            Uri uri = Uri.parse("market://details?id=" + app.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + app.getPackageName())));
            }
        }

    }
}
