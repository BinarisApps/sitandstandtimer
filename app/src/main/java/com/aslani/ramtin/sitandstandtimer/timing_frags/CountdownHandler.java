package com.aslani.ramtin.sitandstandtimer.timing_frags;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.aslani.ramtin.sitandstandtimer.R;

import java.io.IOException;


public class CountdownHandler extends Service {
    private Vibrator vibrator;
    private MediaPlayer mp;

    private CountDownTimer ctimer;
    private CountDownTimer alertTimeOut;
    private boolean isCounting = false;
    private int timeRemaining = 0;
    private int deltaSeconds = 60;
    private int deltaMinutes = 60;
    private int defaultTime;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // logic for switching status
            if (isCounting) // if isCounting means switching from sitting to standing
            {
                ctimer.cancel();
                isCounting = false;
                KillAlert();
                CreateNotification("Status: Standing", Notification.PRIORITY_DEFAULT, false);
                sendMessage("Standing", 0);
                PrefUtils.setInt(getApplicationContext(), 0, "com.icantstandsitting.PAGER_PREF");
                PrefUtils.setBoolean(getApplicationContext(), false, "com.icantstandsitting.FINISHED_PREF");

            } else // Switching from standing to sitting, restart countdown
            {
                StartCountdown(false);
                CreateNotification("Status: Sitting", Notification.PRIORITY_DEFAULT, false);
                sendMessage("Sitting", 0);
                PrefUtils.setInt(getApplicationContext(), 1, "com.icantstandsitting.PAGER_PREF");
            }
        }
    };


    public void StartCountdown(boolean serviceRestarted) {
        CreateNotification("Status: Sitting", Notification.PRIORITY_DEFAULT, false);

        isCounting = true;
        String PrefDefaultTime = PrefUtils.getString(this, "60 Min", "com.icantstandsitting.CountdownLength");
        if (PrefDefaultTime.equals("60 Min")) {
            defaultTime = 3600000;
        } else {
            defaultTime = 1800000;
        }

        // Get timeRemaining from preferences and set this according to boolean
        if (!serviceRestarted) {
            // if service hasn't been restarted use the defaultTime value and update stored value
            timeRemaining = defaultTime;
            PrefUtils.setInt(this, timeRemaining, "com.icantstandsitting.TIME_PREFS");
        } else {
            // else use stored value
            if (PrefUtils.getInt(this, -1, "com.icantstandsitting.TIME_PREFS") != -1) // If value exists in sharedprefs
            {
                timeRemaining = PrefUtils.getInt(this, -1, "com.icantstandsitting.TIME_PREFS"); // assign it to time remaining
            }
        }

        // Countdown 60min with updates every 5s
        ctimer = new CountDownTimer(timeRemaining, 1000) {
            public void onTick(long millisUntilFinished) {
                // Update progress

                timeRemaining = (int) millisUntilFinished;
                // Save in preferences the current time remaining on timer
                PrefUtils.setInt(getApplicationContext(), timeRemaining, "com.icantstandsitting.TIME_PREFS");

                // Update circle with broadcast
                sendMessage("Update Circle", millisUntilFinished);

                // Update text with broadcast
                sendMessage("Update Text", millisUntilFinished);
            }

            public void onFinish() {
                // Countdown is done! Ring the alarm

                // Create notification that handles sound and vibration
                try {
                    ChangeNotificationToAlert();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Save in preference that alarm is ringing (Broadcast for when app in foreground and PrefUtils checked in FragmentSitting for when app is terminated).
                PrefUtils.setBoolean(getApplicationContext(), true, "com.icantstandsitting.FINISHED_PREF");
                // Broadcast message to fragment sitting if active
                sendMessage("Finished", 0);

            }
        }.start();
    }

    // Send an Intent with an action.
    private void sendMessage(String Message, long Extra) {
        Intent intent = new Intent("CountdownEvent");
        // add data
        intent.putExtra("Message", Message);
        intent.putExtra("Extra", Extra);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * The service is starting
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean StopCommand = false;
        if (intent != null) {
            StopCommand = intent.getBooleanExtra("StopCountdown", false);
        }

        if (intent == null && PrefUtils.getInt(this, -1, "com.icantstandsitting.PAGER_PREF") == -1 || PrefUtils.getInt(this, -1, "com.icantstandsitting.PAGER_PREF") == 0) {
            // If the intent is null (i.e restarting service) and the last page was 0 or -1 (i.e) standing. Set StopCommand to avoid starting up the countdown again.
            StopCommand = true;
        }

        if (StopCommand) {
            // We should stop countdown here, not start a new one
            if (isCounting) {
                ctimer.cancel();
                isCounting = false;
                KillAlert();
                CreateNotification("Status: Standing", Notification.PRIORITY_DEFAULT, false);
            }
        } else if (!StopCommand) {
            // Check preferences and see if this is a new service and set time remaining and StartCountdown boolean
            if (!isCounting) {
                if (intent != null) // if intent is not null (i.e service is not being restarted)
                {
                    StartCountdown(false);
                } else {   // else use stored timeRemaining when starting up the countdown again
                    StartCountdown(true);
                }
            }
        }

        return START_STICKY;
    }

    /**
     * Called when The service is no longer used and is being destroyed
     */
    @Override
    public void onDestroy() {
        // Stop the timer
        if (isCounting) {
            ctimer.cancel();
            isCounting = false;
        }
        KillAlert();
        unregisterReceiver(mMessageReceiver);
        RemoveNotification();
        Log.i("COUNTDOWN", "DESTROYED!");
        super.onDestroy();
    }

    // --------------
    // Notification
    // --------------

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
        registerReceiver(mMessageReceiver, new IntentFilter("STATUS"));

        super.onCreate();
    }

    public void CreateNotification(String ContentText, int Priority, boolean showReturnButton) {
        // Creates a persistent notification where the user can change status to restart timer.

        //Define Notification Manager
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int color = ContextCompat.getColor(this, R.color.primary);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.ic_stat_icon_notification)
                .setContentTitle("I Can't Stand Sitting")
                .setContentText(ContentText)
                .setPriority(Priority)
                .setColor(color)
                .setVibrate(new long[0]);

        // Create an explicit intent for an Activity with flags
        Intent resultIntent = new Intent(this, MainTimingActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);

        Intent statusReceive = new Intent("STATUS");
        PendingIntent pendingIntentStatus = PendingIntent.getBroadcast(this, 1, statusReceive, PendingIntent.FLAG_CANCEL_CURRENT);

        mBuilder.addAction(R.drawable.ic_stat_icon_notification, "Change Status", pendingIntentStatus);

        // if boolean true, show Go to app button, else show same notification as FragmentSitting
        if (showReturnButton) {
            mBuilder.addAction(R.drawable.ic_stat_notification_return, "Go to app", pendingIntent);
        }

        // make persistent
        mBuilder.setOngoing(true);

        //Display notification
        notificationManager.notify(9208, mBuilder.build());
    }

    public void ChangeNotificationToAlert() throws IOException {
        // Changes notification to alert user that time is up
        CreateNotification("Enough sitting! Time to stand up!", Notification.PRIORITY_MAX, true);

        // Also starts the vibration and sound which loops until user turns them off through notification or main app.
        // ---------------
        // Vibration
        // ---------------
        long[] pattern = {0, 1000, 500};
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(pattern, 0);
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        // ---------------
        // MEDIA PLAYER
        // ---------------
        mp = new MediaPlayer();
        try {
            mp.setDataSource(this, alert);
            mp.setAudioStreamType(AudioManager.STREAM_RING);
        } catch (IOException | SecurityException | IllegalArgumentException | IllegalStateException e) {
            e.printStackTrace();
        }
        mp.setVolume(100, 100);
        mp.prepare();
        mp.start();
        mp.setLooping(true);

        // Countdown for timeout of vibration and audio, set to 2 minutes

        alertTimeOut = new CountDownTimer(120000, 60000) {
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                KillAlert();
            }
        };
    }

    public void RemoveNotification() {
        // Removes the persistent notification.

        //Define Notification Manager
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public void KillAlert() {
        // Kill vibration and sound.
        if (vibrator != null) {
            vibrator.cancel();
        }
        if (mp != null && mp.isPlaying()) {
            mp.stop();
        }
        if (alertTimeOut != null) {
            alertTimeOut.cancel();
        }
    }


}



